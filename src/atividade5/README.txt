Um produtor, vários consumidores, cada um com o seu buffer,
mas lendo do mesmo buffer compartilhado do servidor.

Produtor	Consumidor
Buffer	Sum	Tem Valor	Estado	Buffer	Sum	Tem Valor	Estado
				 - 	0	falso	Espera
1	1	true	Pronto				
				1	1	Falso	pronto
				-	1	Falso	Espera
				-	1	Falso	Espera
2	3	true	pronto				
				2	3	Falso	pronto
				-	3	falso	Espera
3	6	true	pronto				
				3	6	Falso	pronto
				-	6	falso	espera
4	10	true	pronto				
				4	10	Falso	pronto
				-	10	falso	espera
5	15	true	pronto				
				5	15	Falso	pronto
