/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 19/10/2017
 */
package atividade5;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
